package org.jboss.perf.jca.bench;

import static java.lang.invoke.MethodHandles.publicLookup;
import static java.lang.invoke.MethodType.methodType;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.Method;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.sql.Connection;

public class SecurityActions
{

   static ClassLoader getClassLoader(final Class<?> c)
   {
      if (System.getSecurityManager() == null)
         return c.getClassLoader();

      return AccessController.doPrivileged(new PrivilegedAction<ClassLoader>()
      {
         public ClassLoader run()
         {
            return c.getClassLoader();
         }
      });
   }
   static ClassLoader getThreadContextClassLoader()
   {
      if (System.getSecurityManager() == null)
         return Thread.currentThread().getContextClassLoader();

      return AccessController.doPrivileged(new PrivilegedAction<ClassLoader>()
      {
         public ClassLoader run()
         {
            return Thread.currentThread().getContextClassLoader();
         }
      });
   }

   /**
    * Get the method
    * @param c The class
    * @param name The name
    * @param params The parameters
    * @return The method
    * @exception NoSuchMethodException If a matching method is not found.
    */
   static Method getMethod(final Class<?> c, final String name, final Class<?>... params)
       throws NoSuchMethodException
   {
      if (System.getSecurityManager() == null)
         return c.getMethod(name, params);

      Method result = AccessController.doPrivileged(new PrivilegedAction<Method>()
      {
         public Method run()
         {
            try
            {
               return c.getMethod(name, params);
            } catch (NoSuchMethodException e)
            {
               return null;
            }
         }
      });

      if (result != null)
         return result;

      throw new NoSuchMethodException();
   }

   /**
    * Get the void return no arguments signature MethodHandle
    * @param c The class
    * @param name Method name
    * @return The method Handle or, null if the virtual method does not exist
    */
   static MethodHandle getMethodHandle(final Class<?> c, final String name)
   {
      if (System.getSecurityManager() == null)
      {
         MethodHandles.Lookup lookup = publicLookup();
         MethodType type = methodType(void.class);
         try
         {
            return lookup.findVirtual(c, name, type);
         } catch (NoSuchMethodException|IllegalAccessException e)
         {
            return null;
         }
      }
      else
      {
         return AccessController.doPrivileged(new PrivilegedAction<MethodHandle>() 
         {
            public MethodHandle run()
            {
               try
               {
                  MethodHandles.Lookup lookup = publicLookup();
                  MethodType type = methodType(void.class);
                  return lookup.findVirtual(c, name, type);
               } catch (NoSuchMethodException|IllegalAccessException e)
               {
                  return null;
               }
            }
         });
      }
   }
}
