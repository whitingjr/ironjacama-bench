package org.jboss.perf.jca.bench;

import java.lang.invoke.MethodHandle;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.Optional;

import org.jboss.logging.Logger;

public class WrappedConnection
{
   public void closeWithReflection()
   {
      returnConnectionWithReflection();
   }

   private void returnConnectionWithReflection()
   {
      sqlConnectionNotifyRequestEnd();
   }

   private void sqlConnectionNotifyRequestEnd()
   {
      if (requestEndReflection == null)
      {
         requestEndReflection = lookupNotifyMethod("endRequest");
      }
      if (requestEndReflection.isPresent())
      {
         invokeNotifyMethod(requestEndReflection.get());
      }
   }

   private Optional<Method> lookupNotifyMethod(String methodName)
   {
      try
      {
         Class<?> sqlConnection = getSqlConnection();
         Method method = SecurityActions.getMethod(sqlConnection, methodName, new Class<?>[] {});
         if (method == null)
            return Optional.empty();
         else
            return Optional.of(method);
      } catch (Throwable t)
      {
         if (spy)
         {
            spyLogger.debugf("Unable to invoke java.sql.Connection#%s: %s", methodName, t.getMessage());
         }
         return Optional.empty();
      }
   }

   private void invokeNotifyMethod(Method method)
   {
      try
      {
         method.invoke(mc.getRealConnection());
         if (spy)
         {
            spyLogger.debugf("java.sql.Connection#%s has been invoked", method.getName());
         }
      } catch (Throwable t)
      {
         if (spy)
         {
            spyLogger.debugf("Unable to invoke java.sql.Connection#%s: %s", method.getName(), t.getMessage());
         }
      }
   }

   private Class<?> getSqlConnection() throws SQLException
   {
      Class<?> sqlConnection = null;

      if (sqlConnection == null)
      {
         try
         {
            sqlConnection = Class.forName("java.sql.Connection", true,
                    SecurityActions.getClassLoader(getClass()));
         } catch (Throwable t)
         {
            // Ignore
         }
      }

      if (sqlConnection == null)
      {
         try
         {
            ClassLoader tccl = SecurityActions.getThreadContextClassLoader();
            sqlConnection = Class.forName("java.sql.Connection", true, tccl);
         } catch (Throwable t)
         {
            throw new SQLException("Cannot resolve java.sql.Connection", t);
         }
      }
      return sqlConnection;
   }

   public void closeWithMethodHandle()
   {
      returnConnectionWithMethodHandle();
   }

   private void returnConnectionWithMethodHandle()
   {
      sqlConnectionNotifyRequestEndUsingMethodHandle();
   }

   private void sqlConnectionNotifyRequestEndUsingMethodHandle()
   {
       if (requestEndMH == null)
       {
          requestEndMH = getNativeRepresentation("endRequest");
       }
       if (requestEndMH.isPresent())
          invokeExact(requestEndMH.get(), "endRequest");
   }

   private Optional<MethodHandle> getNativeRepresentation(String methodName)
   {
      try {
         Class<?> sqlConnection = getSqlConnection();
         MethodHandle mh = SecurityActions.getMethodHandle(sqlConnection, methodName);
         if (mh == null)
            return Optional.empty();
         else
            return Optional.of(mh);
      } catch (SQLException e)
      {
         if (spy)
            spyLogger.debugf("Unable to invoke java.sql.Connection#%s: %s", methodName, e.getMessage());
         return Optional.empty();
      }
   }

   private void invokeExact(MethodHandle mh, String methodName)
   {
      try
      {
         mh.invokeExact(mc.getRealConnection());
         if (spy)
            spyLogger.debugf("java.sql.Connection#%s has been invoked", methodName);
      } catch (Throwable t)
      {
         if (spy)
            spyLogger.debugf("Unable to invoke java.sql.Connection#%s: %s", methodName, t.getMessage());
      }
   }

   public WrappedConnection (WrapperManagedConnection manConn)
   {
      mc = manConn;
   }

   private Optional<MethodHandle> requestEndMH;
   private Optional<Method> requestEndReflection;
   protected boolean spy = true;
   protected static Logger spyLogger = Logger.getLogger(org.jboss.perf.jca.bench.WrappedConnection.class.getName());
   private WrapperManagedConnection mc;

}
