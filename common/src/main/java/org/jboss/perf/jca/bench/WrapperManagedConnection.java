package org.jboss.perf.jca.bench;

import java.sql.Connection;

public class WrapperManagedConnection
{
   private Connection conn;
   public WrapperManagedConnection( Connection manCon)
   {
      conn = manCon;
   }
   public Connection getRealConnection()
   {
      return conn;
   }
}
