package org.jboss.perf.jca.bench;

import java.sql.Connection;

import org.jboss.perf.jdbc.JSE8Connection;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

@BenchmarkMode(Mode.Throughput)
@State(Scope.Thread)
public class JSE8IJBench
{
   @Setup
   public void setUp()
   {
      Connection driverconn= new JSE8Connection();
      WrapperManagedConnection manConn = new WrapperManagedConnection(driverconn);
      connection = new WrappedConnection(manConn);
   }
   @Benchmark public void reflectiveJSE8()
   {
      connection.closeWithReflection();
   }
   @Benchmark public void methodHandleJDK8()
   {
      connection.closeWithMethodHandle();
   }

   private WrappedConnection connection;
}
