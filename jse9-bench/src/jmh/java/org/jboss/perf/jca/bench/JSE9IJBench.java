package org.jboss.perf.jca.bench;

import java.sql.Connection;

import org.jboss.perf.jdbc.JSE9Connection;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

@BenchmarkMode(Mode.Throughput)
@State(Scope.Thread)
public class JSE9IJBench
{
   @Setup
   public void setUp()
   {
      Connection driverConn = new JSE9Connection();
      WrapperManagedConnection manConn = new WrapperManagedConnection(driverConn);
      connection = new WrappedConnection(manConn);
   }
   @Benchmark public void reflectiveJSE9()
   {
      connection.closeWithReflection();
   }
   @Benchmark public void methodHandleJSE9()
   {
      connection.closeWithMethodHandle();
   }

   private WrappedConnection connection;
}
