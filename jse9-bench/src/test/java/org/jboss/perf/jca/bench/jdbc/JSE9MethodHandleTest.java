package org.jboss.perf.jca.bench.jdbc;


import org.jboss.perf.jca.bench.WrappedConnection;
import org.jboss.perf.jca.bench.WrapperManagedConnection;
import org.jboss.perf.jdbc.JSE9Connection;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledForJreRange;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.condition.JRE.JAVA_9;

public class JSE9MethodHandleTest
{
   @Test
   @EnabledForJreRange(min = JAVA_9)
   public void reflectiveJSE9()
   {
      connection.closeWithReflection();
      assertEquals(1, driverConnection.getEndCallCount());
   }

   @Test
   @EnabledForJreRange(min = JAVA_9)
   public void methodHandleJSE9()
   {
      connection.closeWithMethodHandle();
      assertEquals(1, driverConnection.getEndCallCount());
   }

   @BeforeEach
   public void setUp()
   {
      driverConnection = new JSE9Connection();
      WrapperManagedConnection manConn = new WrapperManagedConnection(driverConnection);
      connection = new WrappedConnection(manConn);
   }
   @AfterEach
   public void tearDown()
   {
      driverConnection = null;
      connection = null;
   }

   private WrappedConnection connection;
   private JSE9Connection driverConnection;
}
