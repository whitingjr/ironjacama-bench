package org.jboss.perf;

import static java.lang.invoke.MethodHandles.publicLookup;
import static java.lang.invoke.MethodType.methodType;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.api.condition.JRE.JAVA_9;

import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodType;
import java.sql.Connection;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledForJreRange;

public class JSE9ConnectionTest
{
   @Test
   @EnabledForJreRange(min = JAVA_9)
   public void testMethodHandleInvokeExactOnIntefaceMethod()
   {
      Connection conn = new JSE9Connection();
      try {
         MethodType mType = methodType(void.class);
         MethodHandle mh = publicLookup().findVirtual(Connection.class, "endRequest", mType);
         mh.invokeExact(conn);
      } catch (Throwable t)
      {
         fail(t.getClass().getName()+":"+t.getMessage());
      }
   }

   @Test
   public void testMHsize(){
      try {
         MethodType type = methodType(int.class);
         MethodHandle MH_size = publicLookup()
            .findVirtual(java.util.List.class, "size", type);
         int i = (int) MH_size.invokeExact(java.util.Arrays.asList(1,2,3));
      } catch (Throwable t) {
         fail(t.getClass().getName()+":"+t.getMessage());
      }
   }
}
